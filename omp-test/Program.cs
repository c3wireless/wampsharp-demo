﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WampSharp.Core.Listener;
using WampSharp.V2;
using WampSharp.V2.Realm;

public class C3SecBeaconEventData
{
    [JsonProperty("distance")] public float Distance { get; set; }
    [JsonProperty("count")] public uint Count { get; set; }
    [JsonProperty("nonce")] public byte[] Nonce { get; set; }
    [JsonProperty("payload")] public byte[] Payload { get; set; }
    [JsonProperty("tag")] public byte[] Tag { get; set; }
    [JsonProperty("ad")] public byte[] AD { get; set; }
}

public class C3SecBeaconEvent
{
    [JsonProperty("event_type")] public uint EventType { get; set; }

    [JsonProperty("listener_id")] public string ListenerId { get; set; }

    [JsonProperty("data")] public C3SecBeaconEventData Data { get; set; }

    public override string ToString()
    {
        var macAddr = string.Concat(
            Array.ConvertAll(Data.AD.Reverse().ToArray(), x => x.ToString("X2")));

        return $"{ListenerId}: Saw {macAddr} @ {Data.Distance}m";
    }
}

public class C3ListenersEventData
{
    [JsonProperty("name")] public string Name;
    [JsonProperty("version")] public string Version;
    [JsonProperty("model")] public string HardwareModel;
    [JsonProperty("ip_address")] public string IPAddress;
}

public class C3ListenersEvent
{
    [JsonProperty("event_type")] public uint EventType;
    [JsonProperty("data")] public C3ListenersEventData Data;
    
}

namespace omp_test
{
    internal class Program
    {
        private static void ConnectionEstablished(object sender, WampSessionCreatedEventArgs e)
        {
            Console.WriteLine($"Session established: {e.SessionId}");
        }

        private static void ConnectionError(object sender, WampConnectionErrorEventArgs e)
        {
            Console.WriteLine($"A connections error occured: {e.Exception.Message}");
        }

        private static void ConnectionBroken(object sender, WampSessionCloseEventArgs e)
        {
            Console.WriteLine($"Session closed: {e.Reason}");
        }

        public static async Task Main(string[] args)
        {
            Console.WriteLine("Starting WampSharp Test");
            const string location = "ws://ny.labs.c3wireless.com:8080/ws";
            var channelFactory = new DefaultWampChannelFactory();
            var channel = channelFactory.CreateMsgpackChannel(location, "realm1");
            //IWampChannel channel = channelFactory.CreateJsonChannel(location, "realm1");

            await channel.Open().ConfigureAwait(false);
            Console.WriteLine("Channel Open");

            var realmProxy = channel.RealmProxy;
            realmProxy.Monitor.ConnectionError += ConnectionError;
            realmProxy.Monitor.ConnectionEstablished += ConnectionEstablished;
            realmProxy.Monitor.ConnectionBroken += ConnectionBroken;
            
            realmProxy.Services.GetSubject<C3ListenersEvent>(
                "com.c3wireless.listeners").Subscribe(
                x =>
                {
                    if (x.EventType == 0)
                    {
                        Console.WriteLine($"Listener {x.Data.Name} has connected");    
                    }
                },
                exception => { Console.WriteLine($"Exception: {exception.Message} ({exception.Data})"); }); 
            
            realmProxy.Services.GetSubject<C3SecBeaconEvent>(
                "com.c3wireless.listeners.dca63207aef1.secbeacon").Subscribe(
                x => { Console.WriteLine(x); },
                exception => { Console.WriteLine($"Exception: {exception.Message} ({exception.Data})"); });
            
            await Task.Yield();

            Console.WriteLine("Subscriber Registered. <Enter> to quit");

            Console.ReadLine();
        }
    }
}